<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('back.layouts.css')
    {{-- <title>Document</title> --}}
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        @include('back.layouts.aside-menu')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                @include('back.layouts.header')
                <!-- Begin Page Content -->
                    <div class="container-fluid">
                        @yield('content')
                        @yield('content.dashboard')
                    </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
                @include('back.layouts.footer')
        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    @include('back.layouts.js')
</body>
</html>