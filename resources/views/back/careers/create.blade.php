@extends('back.layouts.app') 

@section('content')
    <div class="container">
        <h1>Tambah Data Career</h1>
        <form action="{{route('admin.career.store')}}" method="POST">
            @csrf
                <div class="mb-3">
                    <label class="form-label">Name</label>
                    <input type="text" name="name" placeholder="Masukan Nama" required="" class="form-control">
                </div>
                <div class="mb-3">
                    <label class="form-label">Description</label>
                    <textarea class="form-control" rows="3" name="desc" required=""></textarea>
                </div>
                <div class="mb-3">
                    <label class="form-label">Required</label>
                    <input type="text" name="required" placeholder="Masukan Nama" required="" class="form-control">
                </div>
            <input type="submit" nama="submit" class="btn btn-primary" value="Simpan">
        </form>
    </div>
@endsection