<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Back\DashboardController;
use App\Http\Controllers\Back\CareersController;
use App\Http\Controllers\Back\CategoriesController;
use App\Http\Controllers\Back\PositionsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin', [DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('/admin/dashboard',  [DashboardController::class, 'index'])->name('admin.dashboard');

    //career
    Route::get('/admin/career', [CareersController::class, 'index'])->name('admin.career.index');
    Route::get('/admin/career/add',  [CareersController::class, 'create'])->name('admin.career.create');
    Route::get('/admin/career/edit/{id}',  [CareersController::class, 'edit'])->name('admin.career.edit');
    Route::post('/admin/career/update',  [CareersController::class, 'update'])->name('admin.career.update');
    Route::post('/admin/career/store',  [CareersController::class, 'store'])->name('admin.career.store');
    Route::get('/admin/career/delete/{id}',  [CareersController::class, 'delete'])->name('admin.career.delete');
    
    //category
    Route::get('/admin/category', [CategoriesController::class, 'index'])->name('admin.category.index');
    Route::get('/admin/category/add', [CategoriesController::class, 'create'])->name('admin.category.create');
    Route::get('/admin/category/edit/{id}', [CategoriesController::class, 'edit'])->name('admin.category.edit');
    Route::post('/admin/category/update',  [CategoriesController::class, 'update'])->name('admin.category.update');
    Route::post('/admin/category/store',  [CategoriesController::class, 'store'])->name('admin.category.store');
    Route::get('/admin/category/delete/{id}', [CategoriesController::class, 'delete'])->name('admin.category.delete');
    
    //position
    Route::get('/admin/position', [PositionsController::class, 'index'])->name('admin.position.index');
    Route::get('/admin/position/add', [PositionsController::class, 'create'])->name('admin.position.create');
    Route::get('/admin/position/edit/{id}', [PositionsController::class, 'edit'])->name('admin.position.edit');
    Route::post('/admin/position/update',  [PositionsController::class, 'update'])->name('admin.position.update');
    Route::post('/admin/position/store',  [PositionsController::class, 'store'])->name('admin.position.store');
    Route::get('/admin/position/delete/{id}', [PositionsController::class, 'delete'])->name('admin.position.delete');
    



    }
);
