@extends('back.layouts.app')
@section('content')

    {{-- <div class="container">
        <h1>Tampilan Data Position</h1>
        <div class="my-3 col-12 col-sm-8 col-md-6">
            <form action="" method="GET">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Name" name="keyword">
                    <button class="input-group-text" class="btn btn-primary">Search</button>
                </div>
            </form>
        </div>
        <a href="{{route('admin.position.create')}}" class="btn btn-primary">Tambah</a>
            <table class="table table-sm">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                @foreach ($position as $p)  
                        <tr>
                            <td>{{$p->id}}</td>
                            <td>{{$p->name}}</td>
                            <td>
                                <a href="/admin/position/edit/{{($p->id)}}" class="btn btn-info">Update</a>
                                <a href="/admin/position/delete/{{($p->id)}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                @endforeach
            </table>
    </div> --}}

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables Data Poesition</h1>
    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
        For more information about DataTables, please visit the <a target="_blank"
            href="https://datatables.net">official DataTables documentation</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
        </div>
        <div class="card-body">
            <a href="{{route('admin.position.create')}}" class="btn btn-primary mb-2 ">Tambah</a>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            {{-- <th>ID</th> --}}
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    {{-- <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </tfoot> --}}
                    <tbody>
                        @foreach ($position as $p)  
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                {{-- <td>{{$p->id}}</td> --}}
                                <td>{{$p->name}}</td>
                                <td>
                                    <a href="/admin/position/edit/{{($p->id)}}" class="btn btn-info">Update</a>
                                    <a href="/admin/position/delete/{{($p->id)}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection