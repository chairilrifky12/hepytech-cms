<?php

namespace App\Http\Controllers\Back;

use App\Models\Career;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CareersController extends Controller
{
    public function index()
    {
        $career=Career::all();
        return view('back.careers.index', compact(['career']));
    }

    public function create()
    {
        return view('back.careers.create');
    }

    public function store(Request $request)
    {
        $career = Career::create([
            'name' => $request->name,
            'desc' => $request->desc,
            'required' => $request->required,
        ]);

        return redirect()->route('admin.career.index');
    }

    public function edit($id)
    {
        $career = Career::select('*')
        ->where('id',$id)
        ->get();

        return view('back.careers.edit', ['career' => $career]);
    }

    public function update(Request $request)
    {
        // $career = Career::where('id', $request->id)
        // ->update([
        //     'name' => $request->name,
        //     'desc' => $request->desc,
        //     'required' => $request->required,
        // ]);

        $career = Career::where('id', $request->id)
          ->update([
             'name' => $request->name,
             'desc' => $request->desc,
             'required' => $request->required,
          ]);
        return redirect()->route('admin.career.index');
    }

    public function delete($id)
    {
        $career = Career ::where('id', $id)
          ->delete();

        return redirect()->route('admin.career.index');
    }
}
