@extends('back.layouts.app') 

@section('content')
        <div class="container">
        <h1>Ubah Data Career</h1>
                @foreach ($career as $c) 
                        <form action="{{route('admin.career.update')}}" method="POST">
                        @csrf
                        <div class="mb-3">
                                <label class="form-label">Name</label>
                                <input type="text" name="name" value="{{$c->name}}" placeholder="Masukan Nama" required="" class="form-control">
                        </div>
                        <div class="mb-3">
                                <label class="form-label">Description</label>
                                <textarea name="desc" class="form-control" rows="3" required="">{{$c->desc}}</textarea>
                        </div>
                        <div class="mb-3">
                                <label class="form-label">Required</label>
                                <input type="text" name="required" value="{{$c->required}}" placeholder="Masukan Nama" required="" class="form-control">
                        </div>
                                <input type="hidden" name="id" value="{{ $c->id }}">
                        <input type="submit" nama="submit" class="btn btn-primary" value="Simpan">
                        </form>
                @endforeach
        </div>
@endsection