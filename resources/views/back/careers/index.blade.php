@extends('back.layouts.app')
@section('content')

    {{-- <div class="container">
        <h1>Tampilan Data Career</h1>
        <div class="my-3 col-12 col-sm-8 col-md-6">
            <form action="" method="GET">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Name" name="keyword">
                    <button class="input-group-text" class="btn btn-primary">Search</button>
                </div>
            </form>
        </div>
        <a href="{{route('admin.career.create')}}" class="btn btn-primary">Tambah</a>
        <table class="table table-sm">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Required</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($career as $c)  
                    <tr>
                        <td>{{$c->id}}</td>
                        <td>{{$c->name}}</td>
                        <td>{{$c->desc}}</td>
                        <td>{{$c->required}}</td>
                        <td>
                            <a href="/admin/career/edit/{{($c->id)}}" class="btn btn-info">Update</a>
                            <a href="/admin/career/delete/{{($c->id)}}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div> --}}


    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables Data Career</h1>
    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
        For more information about DataTables, please visit the <a target="_blank"
            href="https://datatables.net">official DataTables documentation</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
        </div>
        <div class="card-body">
            <a href="{{route('admin.career.create')}}" class="btn btn-primary mb-2 ">Tambah</a>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Required</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    {{-- <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Required</th>
                            <th>Action</th>
                        </tr>
                    </tfoot> --}}
                    <tbody>
                        @foreach ($career as $c)  
                            <tr>
                                <td>{{$c->id}}</td>
                                <td>{{$c->name}}</td>
                                <td>{{$c->desc}}</td>
                                <td>{{$c->required}}</td>
                                <td>
                                    <a href="/admin/career/edit/{{($c->id)}}" class="btn btn-info">Update</a>
                                    <a href="/admin/career/delete/{{($c->id)}}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection

