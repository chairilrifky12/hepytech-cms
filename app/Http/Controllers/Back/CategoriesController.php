<?php

namespace App\Http\Controllers\Back;

use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        // $keyword = $request -> keyword;
        // $category = Category::where('name', 'LIKE', '%'.'$keyword'.'%');

        $category = Category::select('*')
        ->get();
        return view('back.category.index', compact(['category']));
    }

    public function create()
    {
        return view('back.category.create');
    }

    public function store(Request $request)
    {
        $category = Category::create([
            'name' => $request->name,
        ]);

        return redirect()->route('admin.category.index');
    }

    public function edit($id)
    {
        $category = Category::select('*')
        ->where('id',$id)
        ->get();

        // $category = DB::table('categories')->where('id',$id)->get();

        return view('back.category.edit', ['category' => $category]);
    }

    public function update(Request $request)
    {

        $category = Category::where('id', $request->id)
          ->update([
             'name' => $request->name
          ]);

        // DB::table('categories')->where('id',$request->id)
        //     ->update([
        //         'name' => $request->name
        // ]);

        return redirect()->route('admin.category.index');
    }

    public function delete($id)
    {
        $category = Category ::where('id', $id)
          ->delete();

        return redirect()->route('admin.category.index');
    }
}
