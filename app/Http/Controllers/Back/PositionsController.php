<?php

namespace App\Http\Controllers\Back;

use App\Models\Position;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PositionsController extends Controller
{
    public function index()
    {
        $position = Position::all();
        return view('back.positions.index', compact(['position']));
    }

    public function create()
    { 
        return view('back.positions.create');
    }

    public function store(Request $request)
    {
        $position = Position::create([
            'name' => $request->name,
        ]);

        return redirect()->route('admin.position.index');
    }

    public function edit($id)
    {
        $position = Position::select('*')
        ->where('id',$id)
        ->get();

        return view('back.positions.edit', ['position' => $position]);
    }

    public function update(Request $request)
    {

        $position = Position::where('id', $request->id)
          ->update([
             'name' => $request->name,
          ]);
        return redirect()->route('admin.position.index');
    }

    public function delete($id)
    {
        $position = Position ::where('id', $id)
          ->delete();

        return redirect()->route('admin.position.index');
    }
}
