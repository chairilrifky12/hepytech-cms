    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('layouts/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('layouts/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('layouts/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('layouts/js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{asset('layouts/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('layouts/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('layouts/js/demo/datatables-demo.js')}}"></script>