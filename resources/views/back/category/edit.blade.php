@extends('back.layouts.app') 

@section('content')
        <div class="container">
            <h1>Ubah Data Category</h1>
                @foreach ($category as $c) 
                <form action="{{route('admin.category.update')}}" method="POST">
                        @csrf
                        {{-- <label>Nama</label> --}}
                        {{-- <input type="text" name="name" value="{{$c->name}}" placeholder="Masukan Nama" required=""><br> --}}
                        <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Name</label>
                                <input type="text" name="name" value="{{$c->name}}" required="required" class="form-control">
                        </div>
                                <input type="hidden" name="id" value="{{ $c->id }}">
                        <input type="submit" nama="submit" class="btn btn-primary" value="Update">
                </form>
                @endforeach
        </div>
@endsection