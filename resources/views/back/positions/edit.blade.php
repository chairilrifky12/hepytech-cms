@extends('back.layouts.app') 

@section('content')
        <div class="container">
                <h1>Ubah Data Position</h1>
                @foreach ($position as $p) 
                        <form action="{{route('admin.position.update')}}" method="POST">
                        @csrf
                        <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Name</label>
                        <input type="text" name="name" value="{{$p->name}}" placeholder="Masukan Nama" required="" class="form-control">
                        </div>
                        <input type="hidden" name="id" value="{{ $p->id }}">
                        <input type="submit" nama="submit" class="btn btn-primary" value="Update">
                        </form>
                @endforeach
        </div>
@endsection
